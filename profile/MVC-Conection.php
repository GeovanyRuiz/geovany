<?php

//NOTA, los parametros que reciben los switch para evaluarlos multiples veces, son los nombres de las acciones (Métodos) (*),

error_reporting(E_ERROR | E_WARNING |E_PARSE); //   <----- Esta linea oculta los warning que se generan por consecuencia de tener todo en una sola vista, quitar cuando se genere algún problema no identificado
require_once('../../models/paths.models.php');

//En esta sección solo incluyo todos los controladores que se creen para que todos los métodos existan.
$controllers[] = scandir(CONTROLLER_PATH);
$quitReturns = array('.', '..');

$paths = array_diff($controllers[0], $quitReturns);

foreach ($paths as $value) {
	include CONTROLLER_PATH.$value;
}

//IMPORTANTE, este switch es usado para métodos personalizados y para guardar registros unicamente, los registros
//que se encarguen de eliminar o actualizar se cargan en otro switch.

switch ($_POST['function']) {// [*]
	case 'saveTab':
		$conf = new config();

		$aplication = explode("-", $_POST['apps-ctm']);
		$object = explode("-", $_POST['objs-ctm']);
		echo $conf->saveTab(array(
				'Name' => $_POST['Name-std'],
				'Label' => $_POST['Name-std'],
				'icon' => $_POST['icon-ctm'],
				'apps' => $aplication[0],
				'objs' => $object[0],
				'CreatedDate' => date("y/m/d")
			));
		break;
		case 'newAssetsType':
			$controller = new assets_type();

			$currency = explode("-", $_POST['Currency-ctm']);

			echo $controller->newAssetsType(array(
				'Name' => 'admin',
				'Description' => $_POST['Description-ctm'],
				'AssetsAccount' => $_POST['AssetsAccount-ctm'],
				'AccountingRate' => $_POST['AccountingRate-ctm'],
				'FiscalRate' => $_POST['FiscalRate-ctm'],
				'CeilingForDeductible' => $_POST['CeilingForDeductible-ctm'],
				'LowOfAssets' => $_POST['LowOfAssets-ctm'],
				'DepreciationMethod' => $_POST['DepreciationMethod-ctm'],
				'Currency' => $currency[0],
				'ProjectedDepreciation' => $_POST['ProjectedDepreciation-ctm'],
				'AssetDepreciation' => $_POST['AssetDepreciation-ctm'],
				'CreatedBy' => 1,
				'CreateDate' => '2016-06-22',
				'LastUpdate' => '2016-06-22'
				)

			);
		break;
	case 'setProject':
			$newProject = new new_project();
			echo $newProject->setProject(array(
				'CreatedBy' => 1,
				'CreatedDate' => date("Y/m/d"),
				'LastUpdate' => date("Y/m/d"),
				'Name' => $_POST['Name-std'],
				'Description' => $_POST['Description-ctm']
				)
			);
		break;

	case 'newRegisterUser':
			$RegisterUser = new create_user();
			$profile = explode("-", $_POST['Profile-ctm']);
			$department = explode("-", $_POST['Department-ctm']);
			$enterprise = explode("-", $_POST['Enterprise-ctm']);

			echo $RegisterUser->newRegisterUser(array(
				'CreatedBy' => 1,
				'CreatedDate' => date("Y/m/d"),
				'LastUpdate' => date("Y/m/d"),
				'Name' => $_POST['Name-std'],
				'Email'=> $_POST['Email-ctm'],
				'Phone'=> $_POST['Phone-ctm'],
				'Birthdate'=> $_POST['Birthdate-ctm'],
				'Position'=> $_POST['Position-ctm'],
				'FirstName'=> $_POST['FirstName-ctm'],
				'LastName'=> $_POST['LastName-ctm'],
				'UserName'=> $_POST['UserName-ctm'],
				'Alias'=> $_POST['LastName-ctm'].$_POST['FirstName-ctm'],
				'Description'=> $_POST['Description-ctm'],
				'Profile'=> $profile[0],
				'Department'=> $department[0],
				'Enterprise'=> $enterprise[0]
				)
			);
		break;

	case 'newInsertCurrency':
			$currency = new select_currency();

			$accounting = explode("-", $_POST['Ins_AccountingPeriodsId-ctm']);
			$currencyCatalog = explode("-", $_POST['Ins_CurrencyCatalogId-ctm']);

			echo $currency->newInsertCurrency(array(
			'CreatedDate' => date("Y/m/d"),
			'Name' => $_POST['Name-std'],
			'Period' => $accounting[0],
			'Currency' => $currencyCatalog[0]
			)
		   );
		break;

	case 'insertDepartament':
			$department = new create_department();
			echo $department->insertDepartament(array(
				'CreatedBy' => 1,
				'CreatedDate' => date("Y/m/d"),
				'LastUpdate' => date("Y/m/d"),
				'Name' => $_POST['Name-std']
				)
			);
		break;

	case 'saveAsset':
		$create_assets = new create_assets();
		echo $create_assets->saveAsset(array(
				'Name' => $_POST['Name-std'],
				'StartDate' => $_POST['StartDate-ctm'],
				'DepreciationMethod' => $_POST['DepreciationMethod-ctm'],
				'MaximumDeductible' => $_POST['MaximumDeductible-ctm'],
				'ClosingAccrued' => $_POST['ClosingAccrued-ctm'],
				'Description' => $_POST['Description-ctm'],
				'AcquisitionDate' => $_POST['AcquisitionDate-ctm'],
				'OriginalAmount' => $_POST['OriginalAmount-ctm'],
				'MarketValue' => $_POST['OriginalAmount-ctm'],
				'AssetsType' => $_POST['AssetsType-ctm'],
				'Currency' => $_POST['Currency-ctm'],
				'Localitation' => $_POST['Localitation-ctm'],
				'Department' => $_POST['Department-ctm'],
				'SerialNumber' => $_POST['SerialNumber-ctm'],
				'AssetsKey' => $_POST['AssetsKey-ctm'],
				'UsefulLife' => $_POST['UsefulLife-ctm'],
				'ScrapValue' => $_POST['ScrapValue-ctm'],
				'Year' => $_POST['Year-ctm'],
				'Month' => $_POST['Month-ctm'],
				'CreatedBy' => 1,
				'Status' => 0,
				'CreatedDate' => date("Y/m/d"),
				'LastUpdate' => date("Y/m/d")
			)
		);
		break;
	case 'newRegisterEnterprise':
		$enterprise = new register_enterprise();
		echo $enterprise->newRegisterEnterprise(array(
			'LastUpdate' => date("Y/m/d"),
			'CreatedDate' => date("Y/m/d"),
			'Name' => $_POST['CreatedBy-std'],
			'CreatedBy' => $_POST['LastUpdate-std'],
			'EnterpriseNum' => $_POST['EnterpriseNum-ctm'],
			'SerialNum' => $_POST['SerialNum-ctm'],
			'SocialReason' => $_POST['SocialReason-ctm'],
			'Address' => $_POST['Address-ctm'],
			'Population' => $_POST['Population-ctm'],
			'RFC' => $_POST['RFC-ctm'],
			'StateRegistration' => $_POST['StateRegistration-ctm'],
			'PersonType' => $_POST['PersonType-ctm'],
			'EnterpriseType' => $_POST['EnterpriseType-ctm'],
			'ConstitutionDate' => $_POST['ConstitutionDate-ctm'],
			'Nationality' => $_POST['Nationality-ctm'],
			'RegimenType' => $_POST['RegimenType-ctm']
			)
		);
		break;
	case 'saveInfo': 
		$profile = new ProfileUserData();
		$profile->saveInfo(array(
			'LastUpdate' => $_POST['Name-std'],
			'CreatedDate' => $_POST['CreatedDate-std'],
			'Name' => $_POST['Name-std'],
			'CreatedBy' => $_POST['LastUpdate-std'],
			'Job' => $_POST['Job-ctm'],
			'Education' => $_POST['Education-ctm'],
			'Skills' => $_POST['Skills-ctm'],
			'URL' => $_POST['URL-ctm']
			)
		); 
		break;

	default:
		//not work
		break;
}


//IMPROTANTE, solo se cargan los métodos encargados de eliminar registros.
switch ($_POST['delete']) {// [*]
	case 'deleteRecord':
			$conf = new config();
			echo $conf->deleteRecord($_POST['recordId']);
		break;

	default:
		# code...
		break;
}

//IMPROTANTE, solo se cargan los métodos encargados de actualizar registros.
switch ($_POST['update']) {// [*]
	case 'editTab':
		$conf = new config();

		$aplication = explode("-", $_POST['apps-ctm']);
		$object = explode("-", $_POST['objs-ctm']);
		echo $conf->editTab(array(
				'Name' => $_POST['Name-std'],
				'Label' => $_POST['Name-std'],
				'icon' => $_POST['icon-ctm'],
				'apps' => $aplication[0],
				'objs' => $object[0],
				'Id' => $_POST['Id']
			));
		break;

	default:
		# code...
		break;
}

/*This method not must removed and updated*/
if (!empty($_POST['searchable'])) {
	echo forms::searchValue($_POST['searchable'], $_POST['post-idField']);
}
?>
