<?php
  require_once('../../models/paths.models.php');
  include CONTROLLER_STC.'forms.php';
  include CONTROLLER_STC.'details.php';
  include CONTROLLER_STC.'editDetails.php';
  include CONTROLLER.'profile_user_data.php';


?>
<body:component>
  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>AYOUI</b> ERP</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Go</b>Life</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $ProfileUserData->getUrl(); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $create_user->getFirstName()." ".$userValidate->getLastName(); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $ProfileUserData->getUrl(); ?>" class="img-circle" alt="User Image">
                <p>
                  <?php echo $create_user->getFirstName()." ".$userValidate->getLastName(); ?>
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="destroySession.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perfil de usuario
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class= "box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo $ProfileUserData->getUrl(); ?>" alt="User profile picture">
              <h3 class="profile-username text-center"><?php echo $create_user->getFirstName()." ".$create_user->getLastName(); ?> </h3>
              <p class="text-muted text-center"> <?php echo $ProfileUserData->getJob(); ?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div><!-- /.box-body -->
          </div><!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
              <p class="text-muted">
                <?php echo $ProfileUserData->getEducation(); ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
              <p class="text-muted"><?php echo $create_user->getLocation(); ?></p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
              <p>
                <span class="label label-success"><?php echo $ProfileUserData->getSkills(); ?></span>
                <span class="label label-danger">UI Design</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class='fa fa-file-text-o margin-r-5'></i> Notes</strong>
              <p><?php echo $create_user->getDescription(); ?></p>
            </div>
          </div>
        </div>
        <!--  AQUI ESTÁ LA CONFIGURACIÓN DEL PERFIL-->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#settings_job" data-toggle="tab">Formación</a></li>
              <li><a href="#settings_location" data-toggle="tab">Locación</a></li>
              <li><a href="#settings_description" data-toggle="tab">Descripción</a></li>
              <li><a href="#settings_photo" data-toggle="tab">Foto de perfil</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Job -->
              <div class="active tab-pane" id="settings_job">
                <form class="form-horizontal" role="form" action="save_job.php" method="post">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Trabajo</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="empleo" placeholder="¿Cuál es tu cargo en la organización?">
                    </div>
                  </div>
                  <div class="form-group">
                    <?php echo "<input type='hidden' name= 'idJob' value='".$_SESSION['idUser']."'>"; ?>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Educación</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="educacion" placeholder="¿En que escuela estudió?">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <!--<input type="checkbox"> I agree to the <a href="#">terms and conditions</a>-->
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Guardar</button>
                    </div>
                  </div>
                </form>
              </div><!-- /.tab-pane -->

              <!-- tab-Location-->
              <div class="tab-pane" id="settings_location">
                <form class="form-horizontal" role="form" action="save_location.php" method="post" >
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Locación</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="locacion" placeholder="Establezca su locación">
                    </div>
                  </div>
                  <div class="form-group">
                    <?php echo "<input type='hidden' name= 'idLocation' value='".$_SESSION['idUser']."'>"; ?>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Guardar</button>
                    </div>
                  </div>
                </form>
              </div><!-- end tab-location-->

              <!-- tab-habílities and description-->
              <div class="tab-pane" id="settings_description">
                <form class="form-horizontal" role="form" action="save_description.php" method="post" >
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Habilidades</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="habilidades" placeholder="habilidades con las que se identifica"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <?php echo "<input type='hidden' name= 'idDescription' value='".$_SESSION['idUser']."'>"; ?>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Descripción</label>
                    <div class="col-sm-10">
                      <textarea type="text" class="form-control" name="descripcion" placeholder="Agregue una descripción o notas"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Guardar</button>
                    </div>
                  </div>
                </form>
              </div>

              <!-- profile photo -->
              <div class="tab-pane" id="settings_photo">
                <form class="form-horizontal" role="form" action="save_photo.php" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Seleccionar foto de perfil</label>
                    <div class="col-sm-10">
                      <input type="file" class="form-control" name="imagen" placeholder="subir foto">
                    </div>
                  </div>
                  <div class="form-group">
                    <?php echo "<input type='hidden' name= 'idPhoto' value='".$_SESSION['idUser']."'>"; ?>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Guardar</button>
                    </div>
                  </div>
                </form>
              </div>

            </div><!-- /.tab-content -->
          </div><!-- /.nav-tabs-custom -->
        </div><!-- /.col -->
      </div><!-- /.row -->

    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

</body:component>
  
  

      