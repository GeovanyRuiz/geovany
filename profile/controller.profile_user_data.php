<?php
	
	require_once('../models/paths.models.php');
	require_once('../models/DML_Actions/m.read.php');
	require_once('m.create.php');
	require_once('m.update.php');
	require_once('m.delete.php');

	class ProfileUserData {

		 public $elements;
		 private $create;
		 private $delete;
		 private $forUpdate;
		 private $read;
		 public $register;
		 public $profileId;
		 public $Job;
		 public $Education;
		 public $Skills;
		 public $URL;

		 function __construct(){

		 }
		
		public function saveInfo($data){
			$this->elements = new StdClass();
			$this->elements->Job = $data['Job'];
			$this->elements->Education = $data['Education'];
			$this->elements->Skills = $data['Skills'];
			$this->elements->URL = $data['URL'];
			$this->create = new read("metadatos", "Ins_Profile", 1);
			return json_encode($this->create->insert($this->elements)); 

		}	

		public function showUserInfo($link){

			$this->read = new read("metadatos", "Ins_Profile", 1);

			$list = $this->read->getList("SELECT Id, Job, Education, Skills, URL FROM Ins_Profile WHERE Id = '".$link."' ");

			if( $list->num_rows > 0 ){
				while ( $this->element = mysql_fetch_assoc($list) ) {
					$this->profileId = $this->element['Id']; 
					$this->Job = $this->element['Id'];
					$this->Education = $this->element['Education'];
					$this->Skills = $this->element['Skills'];
					$this->URL = $this->element['URL'];
				}
			}

			return $this->profileId;
			
		}

		public function editProfile($datos){

			$this->register = new StdClass();
			$this->register->Job = $datos['Job'];
			$this->register->Education = $datos['Education'];
			$this->register->Skills = $datos['Skills'];
			$this->register->URL = $datos['URL'];

			$this->update = new update("metadatos", "Ins_Profile", 1);
			return json_encode($this->forUpdate->update($this->register) );

		}

		public function deleteProfile($link){

			$registro = new StdClass();
			$registro->Id = $link;

			$this->delete = new delete("metadatos", "Ins_Profile", 1);
			return json_encode( $this->delete->delete($registro));

		}

		public function getJob(){
			return $this->Job;
		}

		public function getEducation(){
			return $this->Education;
		}

		public function getSkills(){
			return $this->Skills;
		}

		public function getURL(){
			return $this->URL;
		}

	}	


	$ProfileUserData = new ProfileUserData();
	$ProfileUserData->saveInfo();
?>